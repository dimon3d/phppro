<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'docker_wp1' );

/** MySQL database username */
define( 'DB_USER', 'user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', '192.168.100.156' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'H8wNk@!%NR<r$>gn[|/?uIUm=^4RfG)lcJkp({=b2u]jxo*1cn|rKsHqAGZwz6aR' );
define( 'SECURE_AUTH_KEY',  '#`m:o+#i/(M&ewNBBS_~=:a~g8;*P%n4Zk;!:w0/+V3{.z{EHs D5VjDo7uO fat' );
define( 'LOGGED_IN_KEY',    'd+2,8BmPT@[;~PiKk7R`;*z#Y[Zf;A%JL0Td!sl ALG %VQAadx$Ir-TOBs1jv&k' );
define( 'NONCE_KEY',        'ZV%L87Wqjlsi/TOz5@Us$.nhtgkWaDbVeQ~~ D~)$:JlX1Db_{W|sQjYJEU8Sm>=' );
define( 'AUTH_SALT',        's^M~0RKME;Tqe/;*%C[JZC3c${?w<MHECc|DD3:<Nnd.l*:p]ZTAR3y7Hw(H8WRc' );
define( 'SECURE_AUTH_SALT', '/p|R)WQ+HcHR:^([N{WrKU a[9^Nb?w`i=~T/(]~!Ww!CKv),^[LS;qmRwD*b+DW' );
define( 'LOGGED_IN_SALT',   '8oSN0-6WQAKPQ9}Eg|xLNH,/:K&@anQ=+iB`^H;z-(9FbWeCMf*2tss=RI]G+1Ye' );
define( 'NONCE_SALT',       ']|9j(q HPr@GKFyuZ9Af8qxSlGKR/qSJ/F;uM0l%E84DDWUmyY;G+6@qwN?<Lj3*' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
