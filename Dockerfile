FROM nginx:alpine

COPY ./hosts /etc/nginx/conf.d
COPY ./www /var/www

#WORKDIR /var/www

EXPOSE 80 443

#ENTRYPOINT ["/etc/entrypoint.sh"]
#ENTRYPOINT ["nginx", "-g", "daemon off;"]

